'''
Created on Mar 7, 2013

@author: pim
'''
import sys
import senselet
from senselet.events import senseapi
from senselet.events import commonsense
from  senselet.core import eventMethod, eventExpression, Event
import senselet.activity
import datetime
import time
import json


#Read in oauth token from credentials.json to authenticate with Common Sense
credentials = json.load(open("credentials.json"))
consumer=credentials["auth"]["consumer_key"]
consumer_secret=credentials["auth"]["consumer_secret"]
token=credentials["auth"]["token"]
token_secret=credentials["auth"]["token_secret"]

api = senseapi.SenseAPI()

if "me" in credentials:
    #For running locally, try to login with a session id. This will be done when credentials.json contains: "me":{"user":"","password":""}
    if not api.AuthenticateSessionId(credentials["me"]["user"], senseapi.MD5Hash(credentials["me"]["password"])):
        print "Couldn't login: ".format(api.getResponse())
        sys.exit(1)
else:
    #Currently oauth doesn't work with https, so use http instead
    api.setUseHTTPS(False)
    if not api.AuthenticateOauth(token, token_secret, consumer, consumer_secret):
        print "Couldn't login: ".format(api.getResponse())
        sys.exit(1)

session = commonsense.Session(api)
me = session.me()

def counter():
    """
    Generator to yield (date,count) tuples with a 1 second interval
    """
    i = 0
    while True:
        yield (datetime.datetime.now(),i)
        i += 1
        time.sleep(1)


"""
Create an event with the counter as input that simply saves the counter to a sensor.
"""
description=credentials["parameters"]["parameter1"]
countSensorId = session.createSensorOnce("counter", description, "integer")
Event(inputData=counter()).saveToSensor(session, countSensorId).makeItSo()

"""
Another event that to store wether the user is active.
"""
isActiveId = session.createSensorOnce("isActive", "Example agent", "boolean")
me.event().isActive().saveToSensor(isActiveId).makeItSo()
