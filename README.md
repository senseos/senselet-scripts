#C.S. Agents

#Introduction
C.S. agents are processes that act on behalf of their user. For example they'll
process data a user has collected over the years and provide an interpretation.
Or an agent could monitor something for its user and notify the user when
appropriate.

Basically they're simply processes that run somewhere and have access to their
user's data. They're __loosely coupled__ with the Common Sense so as to allow
them to run anywhere. This makes it very easy to debug and test locally. For
now you can run them somewhere, or even deploy them to [Common Sense Head
Quarters](http://hq.dev.sense-os.nl). Besides normal PC's they can also be run
on embedded devices, for example on a Raspberry PI. In the future they might
even run on your phone.

Since an agent is simply a process it can contact its user via email, notification etc. In order to make the agent also accesible by the user an agent might make a webpage available, called its __dashboard__.

#Structure of an agent
An agent consists of several components. Let's take a look at the example agent, conveniently called Example.

 - Each directory in the root of the repository contains a single agent. The Example agent resides in the directory Example.
 - The description.txt contains information about the agent. Besides information about the agent it also contains the parameters that the agent accepts. When deploying the agent the user will be asked to fill in the parameters.
 - The directory contains a requirements.txt file. This file contains the python dependencies for this agent.
 - The directory also contains a run.py file. This is the python program that contains the actual agent.
 - The directory contains a "www" subdirectory. This is the directory that the dashboard for the agent points to. It contains a webpage that the agent presents to the user.

#Requirements
Since the agents are now in python it's good to know a little bit about python, (pip)[http://www.pip-installer.org] and virtualenv. Don't worry, it's basic information that you can find anywhere on the web, and you might even pick it up while following this guide.

#Creating your own agent.
The easiest way to create your own agent is by copying the example project. For example let's create a new agent called "bond".

```
cp -a Example bond
```

Next modify the description. Change the name to "bond". __Make sure the
directory and your agent's name are the same.__ If you have any parameters for the script put them in the description.txt

Next put your code in the file run.py.
If you have any dependencies put them in requirements.txt. The easiest way to do this is to setup a virtual environment (see: Running an agent locally ) where you install dependencies and then just run

```
pip freeze > requirements.txt
```

An easy way to get started is to use the [eventScripting python package](https://github.com/pimnijdam/eventScripting) which makes it easy to read and write from CommonSense and provides and easy framework for scripting agents.


#Running an agent locally
Upon deployment an extra file is made available to the agent. "credentials.json". This file contains the oauth credentials for the agent and also the parameters the user filled in during deployment. The structure of this file is as follows:

```
{
	"auth": {
		"consumer_key": "",
		"consumer_secret": "",
		"token": "",
		"token_secret": "",
	},
	"parameters": {
		"<parameter name>": "<value>"
	}
}
```

To run an agent locally you should manually create the file. Either generate an oauth token and fill it in, or add another value in the script that you use to authenticate during testing, e.g. a username and password.

Create and activate a virtual environment. (make sure python and virtualenv are installed)

```
virtualenv venv
source venv/bin/activate
```

Install dependencies within the virtual environment:

```
pip install -r requirements.txt
```

Run the agent with the following command:

```
python run.py
```

Once you're done you can deactivate the virtual environment with

```
deactivate
```

Next time you only need to activate your environment again:

```
source venv/bin/activate
```

#How to deploy an agent to C.S. headquarters
To deploy an agent to [C.S. Head Quarters](http://hq.dev.sense-os.nl) make a pull request to this repository. The easiest way is to clone the repository and develop your agent in a separate branch. Then create a pull request into our repository. Currently we're reviewing your agent before we accept your pull request. Once we accept your pull request your agent should be available in C.S. Head Quarters within a few minutes.

##Caveats
 - Right now only Python is supported, more specifically Python version 2.7.
 - The directory name has to be the same as the agent name
 - Don't use spaces in the agent name.

