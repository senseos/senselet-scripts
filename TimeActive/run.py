'''
Created on Mar 7, 2013

@author: pim
'''
from  senselet.core import eventMethod, eventExpression
from datetime import time as dayTime, timedelta
import senselet
from senselet.events import commonsense
import datetime
import json
from senselet.events import senseapi

credentials = json.load(open("credentials.json"))
consumer=credentials["auth"]["consumer_key"]
consumer_secret=credentials["auth"]["consumer_secret"]
token=credentials["auth"]["token"]
token_secret=credentials["auth"]["token_secret"]
api = senseapi.SenseAPI()
api.setUseHTTPS(False)
api.setVerbosity(True)
if "me" in credentials:
	if not api.AuthenticateSessionId(credentials["me"]["user"], senseapi.MD5Hash(credentials["me"]["password"])):
	    print "Couldn't login: ".format(api.getResponse())
else:
	if not api.AuthenticateOauth(token, token_secret, consumer, consumer_secret):
	    print "Couldn't login: ".format(api.getResponse())
session = commonsense.Session(api)
me = session.me()

#define our own condition
@eventExpression("isAbove")
def isAbove(date, value, threshold):
    return value > threshold
@eventExpression("onBelow")
def onBelow(date, value, threshold):
    if value < threshold:
        return value

@eventMethod("sumDailyValue")
def sumDaylyValue(self):
    state={}
    state['value'] = 0
    state ['lastDay'] = None
    def addValue(date, value, state):
        today = date.date()
        if today == state['lastDay']:
            state['value'] += value
        else:
            #another day, reset count
            state['value'] = value
        state['lastDay'] = today
        return state['value']
    self.attach(addValue, state)

@eventMethod("store")
def store(self):
	output = open("www/data.json","w+")
	state={}
	state['lastDay'] = None
	state['value'] = 0
	state['values']=[]
	def doStore(date, value, state):
		today=date.date()
		if today == state['lastDay']:
			state['value'] = value
		else:
			#add value of last day
			state['values'].append([date.date().isoformat(), state['value'] / 3600.0])
			#store values
			output.seek(0)
			print json.dumps({'data':state['values']})
			json.dump({'data':state['values']}, output)
		state['lastDay'] = today

	self.attach(doStore, state)

bigBang=datetime.datetime.fromtimestamp(0)

sensorId = session.createSensorOnce("daily activity time", "daily activity time", "integer")
me.event().isActive().timeTrue().onBelow(60*60).sumDailyValue().saveToSensor(sensorId).store().realTime(30, fromDate=bigBang).makeItSo()
