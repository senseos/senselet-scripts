'''
Created on Mar 26, 2013

@author: pim
'''
from  senselet.core import eventMethod, eventExpression
import datetime
import time
import senselet
from senselet.events import commonsense
import json
from senselet.events import senseapi
import requests

credentials = json.load(open("credentials.json"))
consumer=credentials["auth"]["consumer_key"]
consumer_secret=credentials["auth"]["consumer_secret"]
token=credentials["auth"]["token"]
token_secret=credentials["auth"]["token_secret"]
api = senseapi.SenseAPI()
api.setUseHTTPS(False)
api.setVerbosity(True)
if "me" in credentials:
	if not api.AuthenticateSessionId(credentials["me"]["user"], senseapi.MD5Hash(credentials["me"]["password"])):
	    print "Couldn't login: ".format(api.getResponse())
else:
	if not api.AuthenticateOauth(token, token_secret, consumer, consumer_secret):
	    print "Couldn't login: ".format(api.getResponse())
session = commonsense.Session(api)
me = session.me()

#add prefix to dictionary. TODO: just use a map function for this
def helperDict(data,keyword):
    values = {}
    for (k,v) in data.items():
            values["{}.{}".format(keyword,k)]=v
    return values


@eventExpression("positionToWeather")
def positionToWeather(date, value):
        valueMap = json.loads(value)
        lat, lon = valueMap['latitude'], valueMap['longitude']
        #first find the nearest station
        r = requests.get("http://api.openweathermap.org/data/2.1/find/city?lat={lat}&lon={lon}&cnt=1&APPID=21e29c3f65f7836a6148829b4c7319ef".format(lat=lat, lon=lon))
	#print r.json()
        city = r.json()['list'][0]
	print "{}: Using city {}, with id {} and distance {}.".format(date.isoformat(), city["name"], city["id"], city["distance"])
	
        #get weather at city, stations seems to always return "no data"
        end = time.mktime(date.timetuple())
        start=time.mktime((date - datetime.timedelta(hours=1)).timetuple())
	#r = requests.get("http://api.openweathermap.org/data/2.1/history/station/{stationId}?type=tick&start={start}&end={end}".format(stationId=station['id'],start=start,end=end))
        r = requests.get("http://api.openweathermap.org/data/2.1/history/city/{cityId}?type=tick&start={start}&end={end}&units=metric&APPID=21e29c3f65f7836a6148829b4c7319ef".format(cityId=city['id'],start=start,end=end))
	#print r.json()
        if 'list' in r.json():
            data = r.json()['list'][-1]
	    #convert to nice data structure
	    weather={}
	    weather["latitude"] = lat
	    weather["longitude"] = lon
	    weather["city"] = city["name"]
            weather["main"] = data["weather"][0]["main"]
            weather["description"] = data["weather"][0]["description"]
            weather.update(data["main"])
	    if "wind" in data:
		    weather.update(helperDict(data["wind"],"wind"))
	    if "clouds" in data:
		    weather.update(helperDict(data["clouds"],"clouds"))
	    if "rain" in data:
		    weather.update(helperDict(data["rain"],"rain"))
	    if "snow" in data:
		    weather.update(helperDict(data["snow"],"snow"))
            print weather
            return weather
        else:
            print "NO DATA at {}".format(date)

@eventMethod("onUpdate")
def onUpdate(self):
    state={}
    state['last'] = None
    def onUpdate(date,value,state):
        update = state['last'] is None or date - state['last'] > datetime.timedelta(hours=1)
        if update:
            state['last'] = date
            return value

    self.attach(onUpdate,state)

@eventMethod("personalWeather")
def personalWeather(event):
    #use position as input
    event.sensor("position").onUpdate().positionToWeather()

bigBang=datetime.datetime.fromtimestamp(0)
sensorId = session.createSensorOnce("weather", "personal", "json",{})
me.event().personalWeather().printValue().saveToSensor(sensorId).realTime(10*60, fromDate=bigBang).makeItSo()

