from trollop import TrelloConnection
from collections import OrderedDict
import json

class Trello:
  def __init__(self, key, token):
    self.key = key
    self.token = token
    self.con = TrelloConnection(key, token)

  def getBoardCount(self, board):
    now = self.con.get_board(board)

    # initialize list order by
    lists = now.lists
    lists.sort(key=lambda x:x._data['pos'])

    count = OrderedDict()
    for _list in lists:
      count[_list.name] = {}

    for _list in now.lists:
      list_name = _list.name
      for card in _list.cards:
        if len(card.labels) > 0:
          for label in card.labels:
            lable_name = label['name']
            if not lable_name in count[list_name]:
              count[list_name][lable_name] = 0
            count[list_name][lable_name] += 1

    return count
