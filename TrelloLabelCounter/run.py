'''
Created on Mar 7, 2013

@author: ahmy
'''
import sys
import senselet
from senselet.events import senseapi
from senselet.events import commonsense
from  senselet.core import eventMethod, eventExpression, Event
import senselet.activity
import datetime
import time
import json
from trello import Trello


#Read in oauth token from credentials.json to authenticate with Common Sense
credentials = json.load(open("credentials.json"))
consumer = credentials["auth"]["consumer_key"]
consumer_secret = credentials["auth"]["consumer_secret"]
token = credentials["auth"]["token"]
token_secret = credentials["auth"]["token_secret"]
trello_key = credentials["parameters"]["key"]
trello_token = credentials["parameters"]["token"]
trello_board = credentials["parameters"]["board"]


api = senseapi.SenseAPI()
trello = Trello(trello_key, trello_token)


if "me" in credentials:
    #For running locally, try to login with a session id. This will be done when credentials.json contains: "me":{"user":"","password":""}
    if not api.AuthenticateSessionId(credentials["me"]["user"], senseapi.MD5Hash(credentials["me"]["password"])):
        print "Couldn't login: ".format(api.getResponse())
        sys.exit(1)
else:
    #Currently oauth doesn't work with https, so use http instead
    api.setUseHTTPS(False)
    if not api.AuthenticateOauth(token, token_secret, consumer, consumer_secret):
        print "Couldn't login: ".format(api.getResponse())
        sys.exit(1)

session = commonsense.Session(api)
me = session.me()

def counter():
    """
    Generator to yield (date,count) tuples with a 1 second interval
    """
    i = 0
    while True:
        date = datetime.datetime.now()
        data = trello.getBoardCount(trello_board)
        print "{} {}".format(date, data)
        yield (date, data)
        time.sleep(15 * 60) # 15 minutes


"""
Create an event with the counter as input that simply saves the counter to a sensor.
"""
trelloSensorId = session.createSensorOnce("trello_count", "Trello card label counter", "json")
Event(inputData=counter()).saveToSensor(session, trelloSensorId).makeItSo()