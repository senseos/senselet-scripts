'''
Created on Mar 7, 2013

@author: pim
'''
from  senselet.core import eventMethod, eventExpression
from datetime import time as dayTime, timedelta
import senselet
from senselet.events import commonsense
import datetime
import time
import json
from senselet.events import senseapi

credentials = json.load(open("credentials.json"))
consumer=credentials["auth"]["consumer_key"]
consumer_secret=credentials["auth"]["consumer_secret"]
token=credentials["auth"]["token"]
token_secret=credentials["auth"]["token_secret"]
api = senseapi.SenseAPI()
api.setUseHTTPS(False)
if "me" in credentials:
	if not api.AuthenticateSessionId(credentials["me"]["user"], senseapi.MD5Hash(credentials["me"]["password"])):
	    print "Couldn't login: ".format(api.getResponse())
else:
	if not api.AuthenticateOauth(token, token_secret, consumer, consumer_secret):
	    print "Couldn't login: ".format(api.getResponse())
session = commonsense.Session(api)
me = session.me()

@eventExpression("onBelow")
def onBelow(date, value, threshold):
  if value < threshold:
    return value

@eventMethod("sumDailyValue")
def sumDailyValue(self):
    state={}
    state['value'] = 0
    state ['lastDay'] = None
    def addValue(date, value, state):
        today = date.date()
        if today == state['lastDay']:
            state['value'] += value
        else:
            #another day, reset count
            state['value'] = value
        state['lastDay'] = today
        return state['value']
    self.attach(addValue, state)

@eventMethod("store")
def store(self):
  output = open("www/data.json","w+")

  try:
    json_data = json.load(output)
    allData = json_data['data'][0]
    old_data = dict(zip(allData[0::2],allData[1::2]))
    print "Loaded old data: {}".format(allData)
  except:
    old_data = {}


  delta = 30 #seconds
  state={}
  state['lastUpdate'] = None
  state['values']=old_data
  def doStore(date, value, state):
    state['values'][date.date().isoformat()] = value / 3600.0

    #store to disk every 10 minutes (using time from input)
    if state['lastUpdate'] is None or date - state['lastUpdate'] > timedelta(minutes=10):
      data = state['values'].items()
      data.sort()

      #store values
      output.seek(0)
      print json.dumps({'data': data})
      json.dump({'data': data }, output)
      output.truncate()
      output.flush()
      state['lastUpdate'] = date

  self.attach(doStore, state)

bigBang=datetime.datetime.fromtimestamp(0)

address=credentials["parameters"]["address"]
location = senselet.location.Position(address=address)
sensorId = session.createSensorOnce("time visited", "{}".format(address), "integer")
me.event().isNear(location).timeTrue().onBelow(5*60*60).sumDailyValue().saveToSensor(sensorId).realTime(30, fromDate=bigBang).store().makeItSo()

#me.event().sensor("daily work time").isAbove(9 * 60 * 60).onBecomeTrue().sendMail("pim@sense-os.nl", "Time to relax", "Go home, you've really worked enough for today.").realTime(30).makeItSo()
