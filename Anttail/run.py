'''
Created on Mar 7, 2013

@author: pim
'''
from  senselet.core import eventMethod, eventExpression
from datetime import time as dayTime, timedelta
import senselet
from senselet.events import commonsense
import datetime
import time
import json
from senselet.events import senseapi

credentials = json.load(open("credentials.json"))
consumer=credentials["auth"]["consumer_key"]
consumer_secret=credentials["auth"]["consumer_secret"]
token=credentials["auth"]["token"]
token_secret=credentials["auth"]["token_secret"]
api = senseapi.SenseAPI()
api.setUseHTTPS(False)
#api.setVerbosity(True)

if "me" in credentials:
	if not api.AuthenticateSessionId(credentials["me"]["user"], senseapi.MD5Hash(credentials["me"]["password"])):
	    print "Couldn't login: ".format(api.getResponse())
else:
	if not api.AuthenticateOauth(token, token_secret, consumer, consumer_secret):
	    print "Couldn't login: ".format(api.getResponse())
session = commonsense.Session(api)
greenhouse = session.user("GreenhouseTest")
me = session.me()

bigBang=datetime.datetime.fromtimestamp(0)

maxThreshold=float(credentials["parameters"]["maxThreshold"])

outTemp1 = session.createSensorOnce("anttail_temp_1", "anttail_temp_1", "float")
outTemp2 = session.createSensorOnce("anttail_temp_2", "anttail_temp_2", "float")
outTemp3 = session.createSensorOnce("anttail_temp_3", "anttail_temp_3", "float")
outTemp8 = session.createSensorOnce("anttail_temp_8", "anttail_temp_8", "float")

outTemp7 = session.createSensorOnce("anttail_temp_7", "anttail_temp_7", "float")
outTemp9 = session.createSensorOnce("anttail_temp_9", "anttail_temp_9", "float")
outTemp10 = session.createSensorOnce("anttail_temp_10", "anttail_temp_10", "float")
outTemp12 = session.createSensorOnce("anttail_temp_12", "anttail_temp_12", "float")

outRh = session.createSensorOnce("anttail_rhi", "anttail_rh", "float")
outRh7 = session.createSensorOnce("anttail_rh7", "anttail_rh7", "float")
outRh9 = session.createSensorOnce("anttail_rh9", "anttail_rh9", "float")
outRh10 = session.createSensorOnce("anttail_rh10", "anttail_rh10", "float")
outRh12 = session.createSensorOnce("anttail_rh12", "anttail_rh12", "float")

alertSensor = session.createSensorOnce("anttail_alert", "anttail_alert", "string")

@eventExpression("fix")
def fix(date,value):
	jsonValue=json.loads(value)
	return float(jsonValue["value"])/10.0

@eventExpression("field")
def field(date,value, field):
	jsonValue=json.loads(value)
	return jsonValue[field]

@eventExpression("isAbove")
def isAbove(date,value, threshold):
	return float(value) > threshold

# Import smtplib for the actual sending function
import smtplib

# Import the email modules we'll need
from email.mime.text import MIMEText


def sendAlert():
	# Create a text/plain message
	msg = MIMEText("Temperature exceeds the threshold value of {}.".format(maxThreshold))

	me = '"Sense"<noreply@sense-os.nl>'
	msg['Subject'] = "Temperature alert"
	msg['From'] = me
	msg['To'] = "pim@sense-os.nl"

	# Send the message via our own SMTP server, but don't include the
	# envelope header.
	s = smtplib.SMTP('localhost')
	s.sendmail(me, "pietersix@coldchainconsultants.com", msg.as_string())
	s.quit()

@eventExpression("alert")
def alert(date,value):
    sendAlert()

#scale tempr8,7,9,10,12
greenhouse.event().sensorId(318873).fix().saveToSensor(outTemp8).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(319139).fix().saveToSensor(outTemp7).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(318878).fix().saveToSensor(outTemp9).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(318876).fix().saveToSensor(outTemp10).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(335021).fix().saveToSensor(outTemp12).realTime(10,fromDate=bigBang).makeItSo()
#scale relative humidity, ` 8,7,9,10,12
greenhouse.event().sensorId(318874).fix().saveToSensor(outRh).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(319095).fix().saveToSensor(outRh7).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(318879).fix().saveToSensor(outRh9).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(318877).fix().saveToSensor(outRh10).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(335022).fix().saveToSensor(outRh12).realTime(10,fromDate=bigBang).makeItSo()
#strip temperature for temp 1, 2 and 3
greenhouse.event().sensorId(318450).field("temperature").saveToSensor(outTemp1).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(318452).field("temperature").saveToSensor(outTemp2).realTime(10,fromDate=bigBang).makeItSo()
greenhouse.event().sensorId(318448).field("temperature").saveToSensor(outTemp3).realTime(10,fromDate=bigBang).makeItSo()

#trigger
greenhouse.event().sensorId(318878).fix().isAbove(maxThreshold).onBecomeTrue().attach(lambda x,y:"Alert").saveToSensor(alertSensor).realTime(10, fromDate=bigBang).makeItSo()
print "trigger {}".format(maxThreshold)
greenhouse.event().sensorId(318878).fix().isAbove(maxThreshold).onBecomeTrue().printValue().alert().realTime(5).makeItSo()
